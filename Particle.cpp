/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "Particle.h"

Particle::Particle ( const sf::Vector2f& pos, float mass ) :
    m_Position(pos),
    m_Mass(mass)
{

}


const sf::Vector2f& Particle::GetForce() const
{
    return m_Force;
}
float Particle::GetMass() const
{
    return m_Mass;
}
const sf::Vector2f& Particle::GetPosition() const
{
    return m_Position;
}
const sf::Vector2f& Particle::GetSpeed() const
{
    return m_Speed;
}
void Particle::Move ( float x, float y )
{
    m_Position.x+=x;
    m_Position.y+=y;
    m_Sig_PositionChanged(m_Position);
}
void Particle::Move ( const sf::Vector2f& offset )
{
    Move(offset.x,offset.y);
}
void Particle::SetForce ( const sf::Vector2f& force )
{
    m_Force = force;
}
void Particle::SetMass ( float mass )
{
    m_Mass = mass;
}
void Particle::SetPosition ( float x, float y )
{
    m_Position.x = x;
    m_Position.y = y;
    m_Sig_PositionChanged(m_Position);
}
void Particle::SetPosition ( const sf::Vector2f& pos )
{
    SetPosition(pos.x,pos.y);
}
void Particle::SetSpeed ( const sf::Vector2f& speed )
{
    m_Speed = speed;
}
void Particle::Update ( float elapsedTime )
{
    m_NextPosition.x = m_Position + m_Force.x * elapsedTime * m_Speed.x;
    m_NextPosition.y = m_Position + m_Force.y * elapsedTime * m_Speed.y;
    //TODO: find next position after applying force and speed
}
