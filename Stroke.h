#ifndef STROKE_H
#define STROKE_H

#include <SFML/Graphics.hpp>
#include <deque>

////////////////////////////////////////////////////////////
/// \brief A configurable stroke
///
////////////////////////////////////////////////////////////
class Stroke : public sf::Drawable
{
	public:
		enum SweepStyle
		{
			e_Sin = 0,
			e_Sin2,
			e_Sin3,
			e_SquareSin
		};

	private:
		struct Joint
		{
			Joint();
			sf::Vector2f	position;
			float			birth_time;
			float			thickness;
			float			angle;
			sf::Vector2f	upper_point;
			sf::Vector2f	lower_point;
			sf::Color		inner_color;
			sf::Color		outer_color;
			float			thickness_scale;
		};

		// Datas
		std::deque<Joint>			joints;
		sf::Clock					time;
		sf::Vector2f				joint_linear_speed;

		// Body structure
		unsigned int				max_joints;
		float						max_lenght;
		float						min_segment_lenght;
		float						joint_lifetime;

		// Stroke option
		float						thickness;
		float						start_thickness;	//Range : [0, 1]
		float						end_thickness;		//Range : [0, 1]
		float						stroke_offset;		//Range : [-1, 1]
		float						start_sweep;		//Range : [0, 1]
		float						end_sweep;			//Range : [0, 1]
		int							start_sweep_style;
		int							end_sweep_style;
		float						stippling;			//Range : [0, 1]

		// Appearance
		sf::Color					start_inner_color;
		sf::Color					start_outer_color;
		sf::Color					end_inner_color;
		sf::Color					end_outer_color;
		sf::ResourcePtr<sf::Image>	image;
		sf::IntRect					sub_rect;

		// State
		bool						is_compiled;

		virtual void Render(sf::RenderTarget& target, sf::Renderer& renderer) const;
		void Compile();
		float ComputeThickness(float alpha, float offset, float t);
		float ApplySweep(int sweep_style, float a);
	public:
		// Constructeurs
		Stroke();

		// Accesseurs
		const sf::Vector2f & GetJointLinearSpeed() const;		
		unsigned int GetMaxJoints() const;
		float GetMaxLenght() const;
		float GetLenght() const;
		float GetMinSegmentLenght() const;
		float GetJointLifetime() const;
		float GetThickness() const;
		float GetStartThickness() const;
		float GetEndThickness() const;
		float GetOffset() const;
		float GetStartSweep() const;
		float GetEndSweep() const;
		int GetStartSweepStyle() const;
		int GetEndSweepStyle() const;
		float GetStippling() const;
		const sf::Color & GetStartInnerColor() const;
		const sf::Color & GetStartOuterColor() const;
		const sf::Color & GetEndInnerColor() const;
		const sf::Color & GetEndOuterColor() const;
		const sf::Image * GetImage() const;
		const sf::IntRect & GetSubRect() const;

		// Modificateurs de l'objet
		void SetJointLinearSpeed(float x, float y);									// Set the linear speed of the joints of the stroke. You will need to call the Move() function to make them move.
		void SetJointLinearSpeed(const sf::Vector2f & linear_speed);				// Set the linear speed of the joints of the stroke. You will need to call the Move() function to make them move.
		void SetMaxJoints(unsigned int max_joints);									// Set the maximum joint number a stroke can have. Joints at the end of the stroke will be deleted until the right joint number is reached.
		void SetMaxLenght(float max_lenght);										// Set the maximum lenght the stroke can have. If stroke is too long, joints at the end of the stroke will be deleted untile the good lenght is reached.
		void SetMinSegmentLenght(float min_segment_lenght);							// Set the minimum lenght of each segment between 2 joints. 0 mean their is no limit for the lenght.
		void SetJointLifetime(float joint_lifetime);								// Set the lifetime for each new joint. 0 mean the joint won't die by themselves.
		void SetThickness(float thickness);											// Set the global thickness of the stroke
		void SetStartThickness(float start_thickness);								// Set thickness ratio of the end of the stroke. 'end_thickness' param receive a float between 0 and 1.
		void SetEndThickness(float end_thickness);									// Set thickness ratio of the end of the stroke. 'end_thickness' param receive a float between 0 and 1.
		void SetOffset(float stroke_offset);										// Set the position offset where the stroke begin to decay. 'stroke_offset' param receive a float between -1 (end) and 1 (start).
		void SetSweep(float sweep);													// Set the sweep ratio of the stroke. 'sweep' param receive a float between 0 and 1.
		void SetStartSweep(float start_sweep);										// Set the sweep ratio of the end of the stroke. 'start_sweep' param receive a float between 0 and 1.
		void SetEndSweep(float end_sweep);											// Set the sweep ratio of the end of the stroke. 'end_sweep' param receive a float between 0 and 1.
		void SetSweepStyle(int sweep_style);										// Set the sweep style of the stroke.
		void SetStartSweepStyle(int start_sweep_style);								// Set the sweep style of the start of the stroke.
		void SetEndSweepStyle(int end_sweep_style);									// Set the sweep style of the end of the stroke.
		void SetStippling(float stippling);											// Set the stippling of the stroke. 'stippling' param receive a float between 0 and 1.
		void SetInnerColor(const sf::Color & inner_color);							// Set the inner color for all the stroke.
		void SetOuterColor(const sf::Color & outer_color);							// Set the outer color for all the stroke.
		void SetStartInnerColor(const sf::Color & start_inner_color);				// Set the inner color of the start of the troke.
		void SetStartOuterColor(const sf::Color & start_outer_color);				// Set the outer color of the start of the stroke.
		void SetEndInnerColor(const sf::Color & end_inner_color);					// Set the inner color of the end of the stroke.
		void SetEndOuterColor(const sf::Color & end_outer_color);					// Set the outer color of the end of the stroke.
		void SetImage(const sf::Image & image, bool adjust_to_new_size = false);	// Set the image used as texture (same as sf::Sprite).
		void SetSubRect(const sf::IntRect & sub_rect);								// Set the SubRect of the used image.

		void Clear();																// Remove all joints from the stroke

		bool AddJoint(float x, float y, float thickness_scale = 1, bool precompile = false);				// Add a joint with its own thickness ratio (between 0 and any positive value).
		bool AddJoint(const sf::Vector2f & position, float thickness_scale = 1, bool precompile = false);	// Add a joint with its own thickness ratio (between 0 and any positive value).

		void Move(float frame_time);												// Move all joints according to the linear speed of the stroke.
};

#endif