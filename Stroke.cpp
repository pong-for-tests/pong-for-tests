#include "Stroke.h"
#include "my_utility.h"
#include <iostream>
#include <sstream>

// Constructeur
Stroke::Stroke() :
max_joints(0),
max_lenght(0),
min_segment_lenght(0),
joint_lifetime(0),
thickness(3),
start_thickness(1),
end_thickness(1),
stroke_offset(0),
start_sweep(0),
end_sweep(0),
start_sweep_style(e_Sin),
end_sweep_style(e_Sin),
stippling(0),
start_inner_color(sf::Color::White),
start_outer_color(sf::Color::White),
end_inner_color(sf::Color::White),
end_outer_color(sf::Color::White),
sub_rect(0,0,1,1),
is_compiled(false)
{
}

// Accesseurs
const sf::Vector2f & Stroke::GetJointLinearSpeed() const
{
	return joint_linear_speed;
}

unsigned int Stroke::GetMaxJoints() const
{
	return max_joints;
}

float Stroke::GetMaxLenght() const
{
	return max_lenght;
}

float Stroke::GetLenght() const
{
	float temp_lenght = 0;

	if(joints.size() > 1)
		for(unsigned int i = 1; i < joints.size(); i++)
			temp_lenght += utility::Distance(joints[i-1].position, joints[i].position);

	return temp_lenght;
}

float Stroke::GetMinSegmentLenght() const
{
	return min_segment_lenght;
}

float Stroke::GetJointLifetime() const
{
	return joint_lifetime;
}

float Stroke::GetThickness() const
{
	return thickness;
}

float Stroke::GetStartThickness() const
{
	return start_thickness;
}

float Stroke::GetEndThickness() const
{
	return end_thickness;
}

float Stroke::GetOffset() const
{
	return stroke_offset;
}

float Stroke::GetStartSweep() const
{
	return start_sweep;
}

float Stroke::GetEndSweep() const
{
	return end_sweep;
}

int Stroke::GetStartSweepStyle() const
{
	return start_sweep_style;
}

int Stroke::GetEndSweepStyle() const
{
	return end_sweep_style;
}

float Stroke::GetStippling() const
{
	return stippling;
}

const sf::Color & Stroke::GetStartInnerColor() const
{
	return start_inner_color;
}

const sf::Color & Stroke::GetStartOuterColor() const
{
	return start_outer_color;
}

const sf::Color & Stroke::GetEndInnerColor() const
{
	return end_inner_color;
}

const sf::Color & Stroke::GetEndOuterColor() const
{
	return end_outer_color;
}

const sf::Image * Stroke::GetImage() const
{
	return image;
}

const sf::IntRect & Stroke::GetSubRect() const
{
	return sub_rect;
}

// Modificateurs de l'objet
void Stroke::SetJointLinearSpeed(float x, float y)
{
	SetJointLinearSpeed(sf::Vector2f(x, y));
}

void Stroke::SetJointLinearSpeed(const sf::Vector2f & joint_linear_speed)
{
	this->joint_linear_speed = joint_linear_speed;
}

void Stroke::SetMaxJoints(unsigned int max_joints)
{
	this->max_joints = max_joints;
}

void Stroke::SetMaxLenght(float max_lenght)
{
	this->max_lenght = max_lenght;
}

void Stroke::SetMinSegmentLenght(float min_segment_lenght)
{
	this->min_segment_lenght = min_segment_lenght;
}

void Stroke::SetJointLifetime(float joint_lifetime)
{
	this->joint_lifetime = joint_lifetime;
}

void Stroke::SetThickness(float thickness)
{
	this->thickness = thickness;
	is_compiled = false;
}

void Stroke::SetStartThickness(float start_thickness)
{
	if(start_thickness < 0) start_thickness = 0;
	if(start_thickness > 1) start_thickness = 1;
	this->start_thickness = start_thickness;
	is_compiled = false;
}

void Stroke::SetEndThickness(float end_thickness)
{
	if(start_thickness < 0) start_thickness = 0;
	if(start_thickness > 1) start_thickness = 1;
	this->end_thickness = end_thickness;
	is_compiled = false;
}

void Stroke::SetOffset(float stroke_offset)
{
	if(start_thickness < -1) start_thickness = -1;
	if(start_thickness > 1) start_thickness = 1;
	this->stroke_offset = stroke_offset;
	is_compiled = false;
}

void Stroke::SetSweep(float sweep)
{
	if(sweep < 0) sweep = 0;
	if(sweep > 1) sweep = 1;
	this->start_sweep = sweep;
	this->end_sweep = sweep;
	is_compiled = false;
}

void Stroke::SetStartSweep(float start_sweep)
{
	if(start_sweep < 0) start_sweep = 0;
	if(start_sweep > 1) start_sweep = 1;
	this->start_sweep = start_sweep;
	is_compiled = false;
}

void Stroke::SetEndSweep(float end_sweep)
{
	if(end_sweep < 0) end_sweep = 0;
	if(end_sweep > 1) end_sweep = 1;
	this->end_sweep = end_sweep;
	is_compiled = false;
}

void Stroke::SetSweepStyle(int sweep_style)
{
	this->start_sweep_style = sweep_style;
	this->end_sweep_style = sweep_style;
	is_compiled = false;
}

void Stroke::SetStartSweepStyle(int start_sweep_style)
{
	this->start_sweep_style = start_sweep_style;
	is_compiled = false;
}

void Stroke::SetEndSweepStyle(int end_sweep_style)
{
	this->end_sweep_style = end_sweep_style;
	is_compiled = false;
}

void Stroke::SetStippling(float stippling)
{
	if(stippling < 0) stippling = 0;
	if(stippling > 1) stippling = 1;
	this->stippling = stippling;
}

void Stroke::SetInnerColor(const sf::Color & inner_color)
{
	this->start_inner_color = inner_color;
	this->end_inner_color = inner_color;
	is_compiled = false;
}

void Stroke::SetOuterColor(const sf::Color & outer_color)
{
	this->start_outer_color = outer_color;
	this->end_outer_color = outer_color;
	is_compiled = false;
}

void Stroke::SetStartInnerColor(const sf::Color & start_inner_color)
{
	this->start_inner_color = start_inner_color;
	is_compiled = false;
}

void Stroke::SetStartOuterColor(const sf::Color & start_outer_color)
{
	this->start_outer_color = start_outer_color;
	is_compiled = false;
}

void Stroke::SetEndInnerColor(const sf::Color & end_inner_color)
{
	this->end_inner_color = end_inner_color;
	is_compiled = false;
}

void Stroke::SetEndOuterColor(const sf::Color & end_outer_color)
{
	this->end_outer_color = end_outer_color;
	is_compiled = false;
}

void Stroke::SetImage(const sf::Image & image, bool adjust_to_new_size)
{
	if(!this->image)
		adjust_to_new_size = true;

	if(adjust_to_new_size && (image.GetWidth() > 0) && (image.GetHeight() > 0))
		SetSubRect(sf::IntRect(0, 0, image.GetWidth(), image.GetHeight()));

	this->image = &image;
}

void Stroke::SetSubRect(const sf::IntRect & sub_rect)
{
	this->sub_rect = sub_rect;
}

bool Stroke::AddJoint(float x, float y, float thickness_scale, bool precompile)
{
	return AddJoint(sf::Vector2f(x, y), thickness_scale, precompile);
}

bool Stroke::AddJoint(const sf::Vector2f & position, float thickness_scale, bool precompile)
{
	Joint joint;
	joint.position = position;
	joint.birth_time = time.GetElapsedTime();
	joint.thickness_scale = thickness_scale;

	if(joints.empty())
	{
		joints.push_back(joint);
		return true;
	}

	bool joint_added = false;

	if(min_segment_lenght)
	{
			float angle = utility::Angle2(joints[joints.size()-1].position, position);
			sf::Vector2f last_point_position = joints[joints.size()-1].position;
			int i = 0;
			while(utility::Distance(joints[joints.size()-1].position, position) >= min_segment_lenght/*-1*/)
			{
				joint.position = last_point_position + utility::AngularPosition2(angle, (i + 1) * min_segment_lenght);
				joints.push_back(joint);
				is_compiled = false;
				joint_added = true;
				++i;
			}
	}
	else
	{
		if(joints[joints.size()-1].position != position)
		{
			joints.push_back(joint);
			is_compiled = false;
			joint_added = true;
		}
	}

	if(!joints.empty())
		while((joints.size() > max_joints && max_joints) || (GetLenght() > max_lenght && max_lenght) || (time.GetElapsedTime() - joints[0].birth_time > joint_lifetime && joint_lifetime > 0))
		{
			joints.pop_front();
			is_compiled = false;
			if(joints.empty())
				break;
		}

	if(precompile && joints.size() >= 3)
		Compile();
	return joint_added;
}

void Stroke::Clear()
{
	joints.clear();
}

void Stroke::Move(float frame_time)
{
	if(joint_linear_speed != sf::Vector2f(0, 0))
	{
		for(unsigned int i = 0; i < joints.size(); i++)
		{
			joints[i].position += joint_linear_speed * frame_time;
			joints[i].upper_point += joint_linear_speed * frame_time;
			joints[i].lower_point += joint_linear_speed * frame_time;
		}
	}
}

void Stroke::Render(sf::RenderTarget& target, sf::Renderer& renderer) const
{
	// Do nothing if there are only 2 or less joints
	if(joints.empty() || joints.size() < 3)
		return;

	// Load texture if we use an image
	sf::FloatRect coords;
	if(image)
	{
		coords = image->GetTexCoords(sub_rect);
		renderer.SetTexture(image);
	}
	else
		renderer.SetTexture(NULL);

	if(!is_compiled)
		const_cast<Stroke*>(this)->Compile();

	// Rendering stroke

	if(image)
	{
		for(unsigned int i = 1; i < joints.size(); i++)
		{
			sf::Vector2f center_previous = (!stippling) ? joints[i-1].position : joints[i-1].position * (1.f - stippling/2.f) + joints[i].position * (stippling/2.f);
			sf::Vector2f center_next = (!stippling) ? joints[i].position : joints[i].position * (1.f - stippling/2.f) + joints[i-1].position * (stippling/2.f);
			sf::Vector2f upper_previous = (!stippling) ? joints[i-1].upper_point : joints[i-1].upper_point * (1.f - stippling/2.f) + joints[i].upper_point * (stippling/2.f);
			sf::Vector2f upper_next = (!stippling) ? joints[i].upper_point : joints[i].upper_point * (1.f - stippling/2.f) + joints[i-1].upper_point * (stippling/2.f);
			sf::Vector2f lower_previous = (!stippling) ? joints[i-1].lower_point : joints[i-1].lower_point * (1.f - stippling/2.f) + joints[i].lower_point * (stippling/2.f);
			sf::Vector2f lower_next = (!stippling) ? joints[i].lower_point : joints[i].lower_point * (1.f - stippling/2.f) + joints[i-1].lower_point * (stippling/2.f);

			if(joints[i-1].thickness > joints[i].thickness)
			{
				renderer.Begin(sf::Renderer::TriangleStrip);
					renderer.AddVertex(center_previous.x,	center_previous.y,	coords.Left,				coords.Top + coords.Height,	joints[i-1].inner_color);
					renderer.AddVertex(center_next.x,		center_next.y,		coords.Left + coords.Width,	coords.Top + coords.Height,	joints[i].inner_color);
					renderer.AddVertex(upper_previous.x,	upper_previous.y,	coords.Left,				coords.Top,					joints[i-1].outer_color);
					renderer.AddVertex(upper_next.x,		upper_next.y,		coords.Left + coords.Width,	coords.Top,					joints[i].outer_color);
				renderer.End();

				renderer.Begin(sf::Renderer::TriangleStrip);
					renderer.AddVertex(center_previous.x,	center_previous.y,	coords.Left,				coords.Top + coords.Height,	joints[i-1].inner_color);
					renderer.AddVertex(center_next.x,		center_next.y,		coords.Left + coords.Width,	coords.Top + coords.Height,	joints[i].inner_color);
					renderer.AddVertex(lower_previous.x,	lower_previous.y,	coords.Left,				coords.Top,					joints[i-1].outer_color);
					renderer.AddVertex(lower_next.x,		lower_next.y,		coords.Left + coords.Width,	coords.Top,					joints[i].outer_color);
				renderer.End();
			}
			else
			{
				renderer.Begin(sf::Renderer::TriangleStrip);
					renderer.AddVertex(center_next.x,		center_next.y,		coords.Left + coords.Width,	coords.Top + coords.Height,	joints[i].inner_color);
					renderer.AddVertex(center_previous.x,	center_previous.y,	coords.Left,				coords.Top + coords.Height,	joints[i-1].inner_color);
					renderer.AddVertex(upper_next.x,		upper_next.y,		coords.Left + coords.Width,	coords.Top,					joints[i].outer_color);
					renderer.AddVertex(upper_previous.x,	upper_previous.y,	coords.Left,				coords.Top,					joints[i-1].outer_color);
				renderer.End();

				renderer.Begin(sf::Renderer::TriangleStrip);
					renderer.AddVertex(center_next.x,		center_next.y,		coords.Left + coords.Width,	coords.Top + coords.Height,	joints[i].inner_color);
					renderer.AddVertex(center_previous.x,	center_previous.y,	coords.Left,				coords.Top + coords.Height,	joints[i-1].inner_color);
					renderer.AddVertex(lower_next.x,		lower_next.y,		coords.Left + coords.Width,	coords.Top,					joints[i].outer_color);
					renderer.AddVertex(lower_previous.x,	lower_previous.y,	coords.Left,				coords.Top,					joints[i-1].outer_color);
				renderer.End();
			}
		}
	}
	else
	{
		for(unsigned int i = 1; i < joints.size(); i++)
		{
			sf::Vector2f center_previous = (!stippling) ? joints[i-1].position : joints[i-1].position * (1.f - stippling/2.f) + joints[i].position * (stippling/2.f);
			sf::Vector2f center_next = (!stippling) ? joints[i].position : joints[i].position * (1.f - stippling/2.f) + joints[i-1].position * (stippling/2.f);
			sf::Vector2f upper_previous = (!stippling) ? joints[i-1].upper_point : joints[i-1].upper_point * (1.f - stippling/2.f) + joints[i].upper_point * (stippling/2.f);
			sf::Vector2f upper_next = (!stippling) ? joints[i].upper_point : joints[i].upper_point * (1.f - stippling/2.f) + joints[i-1].upper_point * (stippling/2.f);
			sf::Vector2f lower_previous = (!stippling) ? joints[i-1].lower_point : joints[i-1].lower_point * (1.f - stippling/2.f) + joints[i].lower_point * (stippling/2.f);
			sf::Vector2f lower_next = (!stippling) ? joints[i].lower_point : joints[i].lower_point * (1.f - stippling/2.f) + joints[i-1].lower_point * (stippling/2.f);

			if(joints[i-1].thickness > joints[i].thickness)
			{
				renderer.Begin(sf::Renderer::TriangleStrip);
					renderer.AddVertex(center_previous.x,	center_previous.y,	joints[i-1].inner_color);
					renderer.AddVertex(center_next.x,		center_next.y,		joints[i].inner_color);
					renderer.AddVertex(upper_previous.x,	upper_previous.y,	joints[i-1].outer_color);
					renderer.AddVertex(upper_next.x,		upper_next.y,		joints[i].outer_color);
				renderer.End();

				renderer.Begin(sf::Renderer::TriangleStrip);
					renderer.AddVertex(center_previous.x,	center_previous.y,	joints[i-1].inner_color);
					renderer.AddVertex(center_next.x,		center_next.y,		joints[i].inner_color);
					renderer.AddVertex(lower_previous.x,	lower_previous.y,	joints[i-1].outer_color);
					renderer.AddVertex(lower_next.x,		lower_next.y,		joints[i].outer_color);
				renderer.End();
			}
			else
			{
				renderer.Begin(sf::Renderer::TriangleStrip);
					renderer.AddVertex(center_next.x,		center_next.y,		joints[i].inner_color);
					renderer.AddVertex(center_previous.x,	center_previous.y,	joints[i-1].inner_color);
					renderer.AddVertex(upper_next.x,		upper_next.y,		joints[i].outer_color);
					renderer.AddVertex(upper_previous.x,	upper_previous.y,	joints[i-1].outer_color);
				renderer.End();

				renderer.Begin(sf::Renderer::TriangleStrip);
					renderer.AddVertex(center_next.x,		center_next.y,		joints[i].inner_color);
					renderer.AddVertex(center_previous.x,	center_previous.y,	joints[i-1].inner_color);
					renderer.AddVertex(lower_next.x,		lower_next.y,		joints[i].outer_color);
					renderer.AddVertex(lower_previous.x,	lower_previous.y,	joints[i-1].outer_color);
				renderer.End();
			}
		}
	}
}

void Stroke::Compile()
{
	float total_lenght = GetLenght();

	// Determining joints' angles
	// First joint
	joints[0].angle = utility::Angle2(joints[0].position, joints[1].position);

	// Last joint
	joints[joints.size()-1].angle = utility::Angle2(joints[joints.size()-2].position, joints[joints.size()-1].position);

	// other joints
	for(unsigned int i = 1; i < joints.size()-1; i++)
	{
		// Calculating in and out angles
		float angle1 = utility::Angle2(joints[i-1].position, joints[i].position);
		float angle2 = utility::Angle2(joints[i].position, joints[i+1].position);

		// Calculating joint angle
		joints[i].angle = (angle1 + angle2)/2.f;

		// In certain cases, previous line give the opposite of the wanted angle, applying correction if so
		if(utility::sign(angle1) != utility::sign(angle2) && ((angle1 > 90 || angle1 < -90) || (angle2 > 90 || angle2 < -90)) && (utility::Norme(angle1) + utility::Norme(angle2) > 180))
		{
			if(utility::sign(joints[i].angle) == 1)
				joints[i].angle -= 180;
			else
				joints[i].angle += 180;
		}
	}


	// Compiling outer points, inner points, thickness and color of each joint
	float l = 0;
	for(unsigned int i = 0; i < joints.size(); i++)
	{
		if(i > 0)
			l += utility::Distance(joints[i-1].position, joints[i].position);
		
		float alpha = l / total_lenght;
		float off = (stroke_offset + 1.f)/2.f;
		float t = (thickness/2.f) * joints[i].thickness_scale;

		joints[i].thickness = ComputeThickness(alpha, off, t);
		joints[i].upper_point = joints[i].position + utility::AngularPosition2(joints[i].angle + 90, joints[i].thickness);
		joints[i].lower_point = joints[i].position + utility::AngularPosition2(joints[i].angle - 90, joints[i].thickness);
		joints[i].inner_color = utility::ColorInterpolation(start_inner_color, end_inner_color, alpha);
		joints[i].outer_color = utility::ColorInterpolation(start_outer_color, end_outer_color, alpha);
	}

	is_compiled = true;
}

float Stroke::ComputeThickness(float alpha, float offset, float t)
{
	if(alpha <= offset)
	{
		float a = (alpha * (1.f - end_thickness))/offset + end_thickness;
		if(end_sweep)
		{
			float b = ApplySweep(end_sweep_style, a);
			t *= a * (1.f - end_sweep) + b * end_sweep;
		}
		else
			t *= a;
	}
	else
	{	
		float a = ((alpha - 1.f) * (1.f - start_thickness))/(offset - 1.f) + start_thickness;
		if(start_sweep)
		{
			float b = ApplySweep(start_sweep_style, a);
			t *= a * (1.f - start_sweep) + b * start_sweep;
		}
		else
			t *= a;
	}
	return t;
}

float Stroke::ApplySweep(int sweep_style, float a)
{
	switch(sweep_style)
	{
		case e_Sin:
			return sin(a/2.f * static_cast<float>(utility::pi));
		case e_SquareSin:
			return utility::SquareSin(a/2.f * static_cast<float>(utility::pi));
		case e_Sin2:
			return sin(sin(a/2.f * static_cast<float>(utility::pi))/2.f * static_cast<float>(utility::pi));
		case e_Sin3:
			return sin(sin(sin(a/2.f * static_cast<float>(utility::pi))/2.f * static_cast<float>(utility::pi))/2.f * static_cast<float>(utility::pi));
		default:
			return sin(a/2.f * static_cast<float>(utility::pi));
	}
}

Stroke::Joint::Joint() :
birth_time(0),
thickness_scale(1),
angle(0),
thickness(0)
{
}