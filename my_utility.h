#ifndef MY_UTILITY_H
#define MY_UTILITY_H

#include <SFML/Graphics.hpp>
#include <vector>
#include <cmath>

namespace utility
{
	// Constantes utiles
	const double pi = 3.14159265358979323846;

	/**
	\brief Square root
	 
	\param number : X
	 
	\return square root of X
	*/
	inline float sqrt(float number) {
		long i;
		float x, y;
		const float f = 1.5F;
	 
		x = number * 0.5F;
		y  = number;
		i  = * ( long * ) &y;
		i  = 0x5f3759df - ( i >> 1 );
		y  = * ( float * ) &i;
		y  = y * ( f - ( x * y * y ) );
		y  = y * ( f - ( x * y * y ) ); // si on veut plus de pr�cision
		return number * y;
	}

	/*!
	* \brief Distance entre deux points.
	* \author Hiura
	* \details Cette fonction n�cessite que la fonction sqrt soit d�finie. Il est possible de proc�der ainsi :
	* \code 
	#include <cmath>
	using std::sqrt; // on pourrait utilisez la fonction de Carmack en notant a la place : "using gpl::sqrt;"
	* \endcode
	* \param x1 : abcisse du premier point.
	* \param y1 : ordonn�e du premier point.
	* \param x2 : abcisse du deuxi�me point, par d�faut nul.
	* \param y2 : ordonn�e du deuxi�me point, par d�faut nul.
	* \return Distance entre (x1;y1) et (x2;y2).
	*/
	// Distance entre deux points respectivement de coordonn�es (x1, y1) et (x2, y2)
	inline float Distance(const float x1, const float y1, const float x2 = 0.f, const float y2 = 0.f)
	{
		// Formule de calcul de la distance entre deux points :
		// sqrt( (x1 - x2)� + (y1 - y2)� )
	 
		return sqrt( (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) );
	}
	 
	/*!
	* \brief Distance entre deux points.
	* \author Hiura
	* \param p1 : Premier point.
	* \param p2 : Deuxi�me point, par d�faut nul.
	* \return Distance entre p1 et p2.
	*/
	// Distance entre deux points p1 et p2
	inline float Distance(const sf::Vector2f& p1, const sf::Vector2f& p2 = sf::Vector2f(0.f, 0.f))
	{
		// Utilisation de l'autre fonction Distance.
		return Distance(p1.x, p1.y, p2.x, p2.y);
	}


	// The sign of number
	inline float sign(float number)
	{
		if(number >= 0)
			return 1;
		else
			return -1;
	}

	// Return true if number1 and number2 have different signs
	inline bool DifferentSign(float number1, float number2)
	{
		return sign(number1) != sign(number2);
	}

	// Intersection between the circles C1 and C2
	inline bool Intersect(const sf::Vector3f &c1, const sf::Vector3f &c2)
	{
		return Distance(c1.x, c1.y, c2.x, c2.y) < c1.z + c2.z;
	}

	// Intersection between the P1P2 segment with the C circle
	inline bool Intersect(const sf::Vector2f &P1, const sf::Vector2f &P2, const sf::Vector3f &C)
	{
		float Alpha = (P2.x - P1.x) * (P2.x - P1.x) + (P2.y - P1.y) * (P2.y - P1.y);
		float Beta = 2 * ((P2.x - P1.x) * (P1.x - C.x) + (P2.y - P1.y) * (P1.y - C.y));
		float Gamma = P1.x * P1.x + P1.y * P1.y + C.x * C.x + C.y * C.y - 2 * (P1.x * C.x + P1.y * C.y) - C.z * C.z;

		if((Beta * Beta - 4 * Alpha * Gamma) >= 0)
		{
			float u = ((C.x - P1.x) * (P2.x - P1.x) + (C.y - P1.y) * (P2.y - P1.y)) / (( P2.x - P1.x) * ( P2.x - P1.x) + (P2.y - P1.y) * (P2.y - P1.y));
			return 0 <= u && u <= 1;
		}
		else
		{
			return false;
		}
	}

	// Intersection between the P1P2 segment with the C circle, in inverted Y axis reference
	inline bool Intersect2(const sf::Vector2f &P1, const sf::Vector2f &P2, const sf::Vector3f &C)
	{
		float P1y = -P1.y;
		float P2y = -P2.y;
		float Cy = -C.y;

		float Alpha = (P2.x - P1.x) * (P2.x - P1.x) + (P2y - P1y) * (P2y - P1y);
		float Beta = 2 * ((P2.x - P1.x) * (P1.x - C.x) + (P2y - P1y) * (P1y - Cy));
		float Gamma = P1.x * P1.x + P1y * P1y + C.x * C.x + Cy * Cy - 2 * (P1.x * C.x + P1y * Cy) - C.z * C.z;

		if((Beta * Beta - 4 * Alpha * Gamma) >= 0)
		{
			float u = ((C.x - P1.x) * (P2.x - P1.x) + (Cy - P1y) * (P2y - P1y)) / (( P2.x - P1.x) * ( P2.x - P1.x) + (P2y - P1y) * (P2y - P1y));
			return 0 <= u && u <= 1;
		}
		else
		{
			return false;
		}
	}

	// Intersection of the rect rectangle with the C circle
	inline bool Intersect(const sf::FloatRect &rect, const sf::Vector3f &C)
	{
		if(rect.Contains(C.x, C.y))
			return true;
		if(Distance(rect.Left, rect.Top, C.x, C.y) < C.z
			|| Distance(rect.Left + rect.Width, rect.Top, C.x, C.y) < C.z
			|| Distance(rect.Left + rect.Width, rect.Top + rect.Height, C.x, C.y) < C.z
			|| Distance(rect.Left, rect.Top + rect.Height, C.x, C.y) < C.z)
			return true;
		if(Intersect(sf::Vector2f(rect.Left, rect.Top), sf::Vector2f(rect.Left + rect.Width, rect.Top), C)
			|| Intersect(sf::Vector2f(rect.Left + rect.Width, rect.Top), sf::Vector2f(rect.Left + rect.Width, rect.Top + rect.Height), C)
			|| Intersect(sf::Vector2f(rect.Left + rect.Width, rect.Top + rect.Height), sf::Vector2f(rect.Left, rect.Top + rect.Height), C)
			|| Intersect(sf::Vector2f(rect.Left, rect.Top + rect.Height), sf::Vector2f(rect.Left, rect.Top), C))
			return true;
		return false;
	}

	// Return true if the angle 'angle' is between 'angle_min' and 'angle_max'.
	inline bool Between(float angle, float angle_min, float angle_max)
	{
		if(angle_min < angle_max)
			return angle >= angle_min && angle < angle_max;
		if(angle_min > angle_max)
			return angle <= angle_min && angle > angle_max;
		if(angle_min == angle_max)
			return angle == angle_min;
		return false;
	}

	// Conversion degrees -> radian
	inline float ToRad(float angle)
	{
		return static_cast<float>((angle * pi) / 180.f);
	}

	// Conversion radian -> degrees
	inline float ToDeg(float angle)
	{
		return static_cast<float>((angle * 180.f) / pi);
	}

	// Return the position of a vector starting from the center of the reference, from its direction angle (in degrees) and its distance from the center
	inline sf::Vector2f AngularPosition(float direction, float distance)
	{
		direction = ToRad(direction);
		return sf::Vector2f(distance * static_cast<float>(cos(direction)), distance * static_cast<float>(sin(direction)));
	}

	// Return the position of a vector starting from the center of the reference, from its direction angle (in degrees) and its distance from the center, in inverted Y axis reference
	inline sf::Vector2f AngularPosition2(float direction, float distance)
	{
		direction = ToRad(direction);
		return sf::Vector2f(distance * static_cast<float>(cos(direction)), -distance * static_cast<float>(sin(direction)));
	}

	// Return the Angle between the straight line P1P2 and the X axis, in degree.
	inline float Angle(const sf::Vector2f & P1, const sf::Vector2f & P2)
	{
		if(P1 == P2)
			return 0;
		else
			return ToDeg(atan2(P2.y - P1.y, P2.x - P1.x));
	}

	// Return the angle between the straight line P1P2 and the X axis, in degree, in inverted Y axis reference
	inline float Angle2(const sf::Vector2f & P1, const sf::Vector2f & P2)
	{
		if(P1 == P2)
			return 0;
		else
		{
			return ToDeg(atan2(P1.y - P2.y, P2.x - P1.x));
		}
	}

	// Return the angle of the P vector, in degree
	inline float Angle(const sf::Vector2f & P)
	{
		return ToDeg(atan2(P.y, P.x));
	}

	// Return the angle of the P vector, in degree, in inverted Y axis reference
	inline float Angle2(const sf::Vector2f & P)
	{
		return ToDeg(atan2(-P.y, P.x));
	}

	// Return the positive value of the number
	inline float Norme(float number)
	{
		return number * sign(number);
	}

	// Return the normal (or lenght) of the V vector
	inline float Norme(const sf::Vector2f & V)
	{
		return sqrt(V.x * V.x + V.y * V.y);
	}

	// Return the angle between the V1 and V2 vectors
	inline float EcartAngle(const sf::Vector2f & V1, const sf::Vector2f & V2)
	{
		if(V1 != sf::Vector2f(0,0) && V2 != sf::Vector2f(0,0))
		{
			float Cosinus = (V1.x * V2.x + V1.y * V2.y) / (Norme(V1) * Norme(V2));
			float Sinus = (V1.x * V2.y - V1.y * V2.x);

			return ToDeg(sign(Sinus) * acos(Cosinus));
		}
		else
			return 0;
	}

	// Return the angle between the V1 and V2 vectors, in inverted Y axis reference
	inline float EcartAngle2(const sf::Vector2f & V1, const sf::Vector2f & V2)
	{
		if(V1 != sf::Vector2f(0,0) && V2 != sf::Vector2f(0,0))
		{
			float Cosinus = (V1.x * V2.x + V1.y * V2.y) / (Norme(V1) * Norme(V2));
			if(Cosinus >= 1 || Cosinus <= -1)
				return 0;
			float Sinus = (V1.x * -V2.y + V1.y * V2.x);
			return ToDeg(sign(Sinus) * acos(Cosinus));
		}
		else
			return 0;
	}

	// Return the vector coordinates from its speed and angle
	inline sf::Vector2f ToVector(float angle, float speed = 1.f)
	{
		angle = ToRad(angle);
		return sf::Vector2f(speed * cos(angle), speed * sin(angle));
	}

	// Return the vector coordinates from its speed and angle, in inverted Y axis reference
	inline sf::Vector2f ToVector2(float angle, float speed = 1.f)
	{
		angle = ToRad(angle);
		return sf::Vector2f(speed * cos(angle), speed * -sin(angle));
	}

	// Return the coordinates of the P points after a rotation of 'angle' degrees around the point 'origin'
	inline sf::Vector2f Rotate(const sf::Vector2f & P, float angle, const sf::Vector2f & origin = sf::Vector2f(0,0), const sf::Vector2f & scale = sf::Vector2f(1,1))
	{
		angle = ToRad(angle);
		return sf::Vector2f((P.x * cos(angle) - P.y * sin(angle)) * scale.x + origin.x, (-P.x * sin(angle) - P.y * cos(angle)) * scale.y + origin.y);
	}

	// Return the coordinates of the V vector after a vectorial rotation of 'a' degrees
	inline sf::Vector2f VectorialRotation(const sf::Vector2f & V, float a)
	{
		a = ToRad(a);
		return sf::Vector2f(V.x * cos(a) - V.y * sin(a), V.x * sin(a) + V.y * cos(a));
	}

	// Return the position of a point on the Bezier curve, formed by the extremities P0 and P3 and the control points P1 and P2, at a 't' position on the curve (with 't' between 0 and 1)
	inline sf::Vector2f Bezier(const sf::Vector2f & P0, const sf::Vector2f & P1, const sf::Vector2f & P2, const sf::Vector2f & P3, float t)
	{
		sf::Vector2f result;

		result.x = P0.x * (-(t*t*t) + 3*(t*t) - 3*t + 1) + P1.x * ( 3*(t*t*t) - 6*(t*t) + 3*t) + P2.x * (-3*(t*t*t) + 3*(t*t)) + P3.x * (t*t*t);
		result.y = P0.y * (-(t*t*t) + 3*(t*t) - 3*t + 1) + P1.y * ( 3*(t*t*t) - 6*(t*t) + 3*t) + P2.y * (-3*(t*t*t) + 3*(t*t)) + P3.y * (t*t*t);

		return result;
	}

	// Return the position of a point on the Bezier curve, formed by the extremities P0 and P3 and the control point P1, at a 't' position on the curve (with 't' between 0 and 1)
	inline sf::Vector2f Bezier(const sf::Vector2f & P0, const sf::Vector2f & P1, const sf::Vector2f & P2, float t)
	{
		sf::Vector2f result;

		result.x = P0.x * ( 3*(t*t*t) - 6*(t*t) + 3*t) + P1.x * (-3*(t*t*t) + 3*(t*t)) + P2.x * (t*t*t);
		result.y = P0.y * ( 3*(t*t*t) - 6*(t*t) + 3*t) + P1.y * (-3*(t*t*t) + 3*(t*t)) + P2.y * (t*t*t);

		return result;
	}

	// Return the position of a point on the Spline curve, formed by the extremities P0 and P3 and the control points P1 and P2, at a 't' position on the curve (with 't' between 0 and 1)
	inline sf::Vector2f Spline(const sf::Vector2f & P0, const sf::Vector2f & P1, const sf::Vector2f & P2, const sf::Vector2f & P3, float t)
	{
		sf::Vector2f result;

		result.x = P0.x * (2*(t*t*t) - 3*(t*t) + 1) + P1.x * ((t*t*t) - 2*(t*t) + t) + P3.x * (-2*(t*t*t) + 3*(t*t)) + P2.x * ((t*t*t) - (t*t));
		result.y = P0.y * (2*(t*t*t) - 3*(t*t) + 1) + P1.y * ((t*t*t) - 2*(t*t) + t) + P3.y * (-2*(t*t*t) + 3*(t*t)) + P2.y * ((t*t*t) - (t*t));

		return result;
	}

	// Return the convertion of an array of non-equidistant Bezier points to an array of equidistant Bezier point of the same size
	inline std::vector<sf::Vector2f> ResampleBezier(const std::vector<sf::Vector2f> & points)
	{
		/* Tableau des longueurs */
		std::vector<float> longueur;
		longueur.resize(points.size());

		longueur[0] = 0;
		for(int i = 1; i < (int)longueur.size(); i++)
		{
			longueur[i] = longueur[i-1] + Distance(points[i-1], points[i]);
			//std::cout << "Longueur " << i << " : " << longueur[i] << std::endl;
		}

		/* espacement id�al entre deux points */
		float espacement = longueur[longueur.size()-1]/(float)(longueur.size()-1);

		/* Calcul de la nouvelle courbe */
		std::vector<sf::Vector2f> newpoints;
		newpoints.resize(points.size());

		int j = 1;
		for(int i = 0; i < (int)newpoints.size()-1; i++)
		{
			float longueur_ideale = i*espacement;
			while(longueur[j] < longueur_ideale) j++;

			float long_p = longueur[j-1];
			float long_n = longueur[j];
			float ratio = (longueur_ideale - long_p) / (long_n - long_p);
			sf::Vector2f p;
			p.x = (1-ratio) * points[j-1].x + ratio * points[j].x;
			p.y = (1-ratio) * points[j-1].y + ratio * points[j].y;
			newpoints[i] = p;
		}
		newpoints[newpoints.size()-1] = points[points.size()-1];
		return newpoints;
	}

	// Return the interpolation of color1 and color2 with the given 'alpha'
	inline sf::Color ColorInterpolation(const sf::Color & color1, const sf::Color & color2, float alpha)
	{
		return sf::Color(static_cast<sf::Uint8>(static_cast<float>(color1.r) * alpha + static_cast<float>(color2.r) * (1.f - alpha)),
			static_cast<sf::Uint8>(static_cast<float>(color1.g) * alpha + static_cast<float>(color2.g) * (1.f - alpha)),
			static_cast<sf::Uint8>(static_cast<float>(color1.b) * alpha + static_cast<float>(color2.b) * (1.f - alpha)),
			static_cast<sf::Uint8>(static_cast<float>(color1.a) * alpha + static_cast<float>(color2.a) * (1.f - alpha)));
	}
	
	// return the ceil of the 'vec' vector
	inline sf::Vector2f Ceil(const sf::Vector2f & vec)
	{
		return sf::Vector2f(ceil(vec.x), ceil(vec.y));
	}

	// Return the square sinus of the angle 'angle', in radian
	inline float SquareSin(float angle)
	{
		angle = sin(angle);
		return angle * angle;
	}
}
#endif