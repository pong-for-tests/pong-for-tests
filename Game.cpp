/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "Game.h"
#include "Player.h"

Game::Game()
{
    m_BallEffect.SetMaxLenght(150);
    m_BallEffect.SetThickness(60);
    m_BallEffect.SetMinSegmentLenght(5);
    m_BallEffect.SetOffset(0.75f);
    m_BallEffect.SetStartThickness(0.15f);
    m_BallEffect.SetEndThickness(0);
    m_BallEffect.SetSweep(1);
    m_BallEffect.SetStartSweepStyle(Stroke::e_Sin);
    m_BallEffect.SetEndSweepStyle(Stroke::e_SquareSin);
    m_BallEffect.SetStartInnerColor(sf::Color(255, 255, 0));
    m_BallEffect.SetEndInnerColor(sf::Color(255,128,0,0));
    m_BallEffect.SetOuterColor(sf::Color(255, 0, 0, 0));

    m_BallEffect.SetBlendMode(sf::Blend::Add);

    BallPositionChanged.connect(sigc::mem_fun(*this, &Game::OnBallPositionChanged));
    m_BallRadius = 20.f;
    m_BallImage.LoadFromFile("ball.png");

    m_BallSprite.SetImage(m_BallImage);
    m_BallSprite.Resize(m_BallRadius*5,m_BallRadius*5);
    m_BallSprite.SetOrigin(m_BallImage.GetWidth()/2,m_BallImage.GetHeight()/2);
    Rewind();
}

void Game::OnBallPositionChanged ( const sf::Vector2f& pos )
{
    m_BallEffect.AddJoint(pos,sf::Randomizer::Random(0.8f,1.4f));
    m_BallSprite.SetPosition(pos);
}


void Game::SetPlayer(const Game::PlayerType& type, Player& player)
{
    if(type == FirstPlayer)
        m_Player1 = &player;
    else
        m_Player2 = &player;
}

void Game::AddSpectator(Player& player)
{
    m_Spectators.push_back(&player);
}

void Game::DeleteSpectator(Player& player)
{
    m_Spectators.remove(&player);
}


void Game::SetPool(const sf::FloatRect& pool)
{
    m_Pool = pool;
    m_Center = sf::Vector2f(m_Pool.Left + (m_Pool.Width/2),
                            m_Pool.Top  + (m_Pool.Height/2));
}

void Game::Rewind()
{
    m_BallPosition = m_Center;
    BallPositionChanged(m_BallPosition);
    m_BallForce = sf::Vector2f(static_cast<float>(sf::Randomizer::Random(100,-100)), static_cast<float>( sf::Randomizer::Random(100,-100)));

    m_BallSpeed = 0.5f;
}

void Game::Run(float elapsedTime)
{
    sf::Vector2f ballPosition = m_BallPosition;
    ballPosition.x += m_BallForce.x * elapsedTime * m_BallSpeed;
    ballPosition.y += m_BallForce.y * elapsedTime * m_BallSpeed;

    if(ballPosition.y <= m_Pool.Top+m_BallRadius) {
        try{
            SoundManager::PlaySound("ball_wall_hit");
        }
        catch(...){}
        ballPosition.y = m_Pool.Top  + m_BallRadius;
        m_BallForce.y *=-1;
    }
    else if(ballPosition.y >= (m_Pool.Top+m_Pool.Height-m_BallRadius)) {
        try {
            SoundManager::PlaySound("ball_wall_hit");
        }
        catch(...){}
        ballPosition.y = m_Pool.Top + m_Pool.Height - m_BallRadius;
        m_BallForce.y *=-1;
    }
    else if(ballPosition.x <= m_Pool.Left+m_BallRadius) {
        try {
            SoundManager::PlaySound("ball_wall_hit");
        }
        catch(...){}
        ballPosition.x = m_Pool.Left + m_BallRadius;
        m_BallForce.x *=-1;
    }
    else if(ballPosition.x >= m_Pool.Left+m_Pool.Width-m_BallRadius) {
        try {
            SoundManager::PlaySound("ball_wall_hit");
        }
        catch(...){}
        m_BallForce.x *=-1;
    }


    m_BallPosition = ballPosition;
    m_BallSpeed += 0.01 * elapsedTime;

    BallSpeedChanged.emit(m_BallSpeed);
    BallPositionChanged.emit(m_BallPosition);

    m_BallEffect.Move(elapsedTime);
}

bool Game::Update(const sf::Event& event)
{
    return false;
}

void Game::Render(sf::RenderTarget& target)
{
    float right = m_Pool.Left+m_Pool.Width;
    float bottom = m_Pool.Top+m_Pool.Height;
    target.Draw(sf::Shape::Line(m_Pool.Left,m_Pool.Top, m_Pool.Left,bottom,10.f,sf::Color::White));
    target.Draw(sf::Shape::Line(right,m_Pool.Top, right,bottom,10.f,sf::Color::White));
    target.Draw(sf::Shape::Line(m_Pool.Left,m_Pool.Top, right,m_Pool.Top,10.f,sf::Color::White));
    target.Draw(sf::Shape::Line(m_Pool.Left, bottom, right,bottom,10.f,sf::Color::White));

    int repetition = static_cast<int>((m_Pool.Height) / 20);

    for(int i = 0; i < repetition; i++) {
        target.Draw(sf::Shape::Line(m_Pool.Left, bottom-10.f-i*20.f, right,bottom-10.f-i*20.f,10.f,sf::Color::Black));
    }

    target.Draw(m_BallSprite);
    target.Draw(m_BallEffect);
}

