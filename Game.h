/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include <list>
#include <iostream>
#include <sigc++/signal.h>
#include <sigc++/bind.h>
#include "Stroke.h"
#include "SoundManager.h"

class Player;

class Game {
public:

    enum PlayerType { FirstPlayer, SecondPlayer};

public:
    Game();

    void Rewind();
    void Run(float elapsedTime);

    void SetPlayer(const PlayerType& type, Player& player);
    void AddSpectator(Player& player);
    void DeleteSpectator(Player& player);

    void OnBallPositionChanged(const sf::Vector2f& pos);

    void SetPool(const sf::FloatRect& pool);

    bool Update(const sf::Event& event);
    void Render(sf::RenderTarget& target);

    sigc::signal<void,PlayerType,Player*> PlayerChanged;
    sigc::signal<void,PlayerType,Player*> PlayerPointsChanged;
    sigc::signal<void,float> BallSpeedChanged;
    sigc::signal<void,sf::Vector2f> BallPositionChanged;
private:
    void AddCollisionEffect(const sf::Vector2f& pos);

    sf::FloatRect m_Pool;
    Player *m_Player1, *m_Player2;
    std::list<Player*> m_Spectators;
    sf::Vector2f m_Center,
                m_BallPosition,
                m_BallForce;
    float       m_BallSpeed,
                m_BallRadius;
    Stroke      m_BallEffect;
    sf::Sprite  m_BallSprite;

    sf::Image m_CollisionImage;
    sf::Image m_BallImage;
};

#endif // GAME_H
