/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef PARTICLE_H
#define PARTICLE_H

#include <SFML/System.hpp>
#include <sigc++/signal.h>

class Particle {

public:
                        Particle(const sf::Vector2f& pos=sf::Vector2f(0.f,0.f), float mass=1.f);
    void                Move(float x, float y);
    void                Move(const sf::Vector2f& offset);
    void                SetPosition(float x, float y);
    void                SetPosition(const sf::Vector2f& pos);
    void                SetSpeed(const sf::Vector2f& speed);
    void                SetForce(const sf::Vector2f& force);
    void                SetMass(float mass);
    const sf::Vector2f& GetPosition() const;
    const sf::Vector2f& GetSpeed() const;
    const sf::Vector2f& GetForce() const;
    float               GetMass() const;
    void                Update(float timeLapse);



private:
    sf::Vector2f        m_Force;
    sf::Vector2f        m_Speed;
    sf::Vector2f        m_Position,
                        m_NextPosition;
    float m_Mass;


    sigc::signal<void,sf::Vector2f>     m_Sig_PositionChanged;
    sigc::signal<void,Particle*>        m_Sig_CollisionDetected;
};

#endif // PARTICLE_H
