#include <iostream>
#include <SFML/Graphics.hpp>

#include "Game.h"
#include "Player.h"
#include "Converter.h"
#include "SoundManager.h"

namespace {
    sf::Text speed;
}

void ChangeBallSpeed(float bs) {
    speed.SetString(Converter::ToString(bs*100));
}

int main(int argc, char **argv) {

    SoundManager::AddSound("ball_wall_hit","ball_wall_hit.ogg");

    speed.SetPosition (350.f, 550.f);
    sf::RenderWindow window;
    sf::Event event;
    sf::Clock clock;
    float lastTime = clock.GetElapsedTime();

    Game game;
        game.SetPool(sf::FloatRect( 20,20,760,400 ));
        game.BallSpeedChanged.connect(sigc::ptr_fun(&ChangeBallSpeed));
        game.Rewind();


    Player player1;
        game.SetPlayer(Game::FirstPlayer,player1);
    Player player2;
        game.SetPlayer(Game::SecondPlayer,player2);




    window.Create(sf::VideoMode(800,600,32),"Pong");
        window.SetFramerateLimit(60);

    while(window.IsOpened()) {
        while(window.GetEvent(event)) {
            if(event.Type == sf::Event::Closed)
                window.Close();

            else
                game.Update(event);
        }
        game.Run(clock.GetElapsedTime()-lastTime);
        lastTime = clock.GetElapsedTime();

        window.Clear();
            game.Render(window);
            window.Draw(speed);
        window.Display();
    }
    return 0;
}
